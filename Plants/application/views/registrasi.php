<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login | Register</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url() ?>assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>
<body class="bg-light">
    <div class="container" >
        <div class="row justify-content-center">
            <div class="col-xl-9 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block">
                    <img style="height: 28em; width: 20em;" src="<?php echo base_url() ?>/assets/img/login.jpeg" alt="logo" class="img-logo">
                    </div>
                    <div class="col-lg-6">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Buat Akun Baru!</h1>
                            </div>
                            <form method="post" action="" class="user">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="exampleInputNama"
                                        placeholder="Nama Lengkap" name="nama">
                                    <?php echo form_error('nama', '<div class="text-danger small">', '</div>');?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="exampleInputUsername"
                                        placeholder="Username" name="username">
                                    
                                    <?php echo form_error('username', '<div class="text-danger small">', '</div>');?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user"
                                            id="exampleInputPassword" placeholder="Password" name="password1">
                                        <?php echo form_error('password1', '<div class="text-danger small">', '</div>');?>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user"
                                            id="exampleRepeatPassword" placeholder="Ulangi Password" name="password2">
                                        <?php echo form_error('password2', '<div class="text-danger small">', '</div>');?>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Daftar</button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="<?php echo base_url('auth/login')?>">Sudah Punya Akun? Masuk.</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>