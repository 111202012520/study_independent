<?php
class Registrasi extends CI_Controller{
    
    public function index(){

        $this->form_validation->set_rules('nama', 'Nama', 'required', [
            'required' => 'Nama Lengkap wajib diisi!'
        ]);
        $this->form_validation->set_rules('username', 'Username', 'required', [
            'required' => 'Username wajib diisi!'
        ]);
        $this->form_validation->set_rules('password1', 'Password1', 'required|matches[password2]', [
            'required' => 'Password wajib diisi!',
            'matches'  => 'Password tidak cocok!'
        ]);
        if($this->form_validation->run() == FALSE){

            $this->load->view('templates/header');
            $this->load->view('registrasi');
            $this->load->view('templates/footer');
        }else{
            $data = array(
                'id_user'   => '',
                'nama'      => $this->input->post('nama'),
                'username'  => $this->input->post('username'),
                'password'  => $this->input->post('password1'),
                'level'     => 2,
            );
            $this->db->insert('user', $data);
            redirect('auth/login');
        }

    }
}